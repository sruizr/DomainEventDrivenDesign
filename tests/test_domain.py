from dedd.domain import Value
import tests.given as given


class FeatureAgregateNotifyEvents:
    def ucase_aggregate_notify_events_since_beginning(self):
        aggregate = given.A_BasicAggregate()
        observer = given.A_SimpleEventObserver()
        aggregate.add_observer(observer)
        assert not observer.observed_entities

        aggregate.create()
        id = aggregate.id
        assert aggregate.counter == 0
        assert observer.observed_entities[id][0][0] == 0
        assert (observer.observed_entities[id][0][1] ==
                aggregate.CounterInitialized(0))

        aggregate.increase(1)
        assert aggregate.counter == 1
        assert len(observer.observed_entities[id]) == 2
        assert observer.observed_entities[id][1][0] == 1
        assert (observer.observed_entities[id][1][1] ==
                aggregate.CounterIncreased(1))

    def ucase_aggregate_notify_previous_events(self):
        aggregate = given.A_BasicAggregate()
        observer = given.A_SimpleEventObserver()

        aggregate.create()
        assert not observer.observed_entities

        aggregate.add_observer(observer)

        id = aggregate.id
        assert aggregate.counter == 0
        assert observer.observed_entities[id][0][0] == 0
        assert (observer.observed_entities[id][0][1] ==
                aggregate.CounterInitialized(0))


class FeatureEntityIdentification:
    pass


class FeatureValuesAreInmutables:
    def ucase_two_values_with_same_pars_are_inmutables(self):
        value = Value(par1=12, par2='abc')
        other_value = Value(par1=12, par2='abc')

        assert value == other_value

        diferent_value = Value(par1=12, par3='abc')
        assert value != diferent_value


class FeatureValuesAreLegibles:
    def ucase_values_are_legible_with_basic_parameters(self):
        value = Value(par1=12, par2='abc')
        assert repr(value) == 'Value(par1=12, par2=\'abc\')'
