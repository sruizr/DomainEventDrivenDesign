from unittest.mock import Mock, call, patch
from pytest import mark
from dedd.domain import Aggregate
from dedd.eventsource import EventSource


class FakeAggregate(Aggregate):
    def __init__(self):
        super().__init__()
        self.id = 0

        self.raise_(Mock(name='Event1'))
        self.raise_(Mock(name='Event2'))
        self.raise_(Mock(name='Event3'))


class FakeEventStore:
    def __init__(self):
        pass


@mark.current
class FeatureEventSourceStoresAggregates:

    def _given_an_event_source(self):
        event_store = Mock()
        broker = Mock()
        return EventSource(event_store, broker)

    def ucase_entity_events_are_saved(self):
        entity = FakeEntity()
        event_source = self._given_an_event_source()

        event_source.save(entity)

        expected_calls = []
        for event_record in entity._event_records:
            expected_calls.append(
                call(
                    ('FakeEntity', entity.id, entity._version, event.plain_args))
                )

        assert event_source.event_store.save.mock_calls == expected_calls


    def ucase_entity_events_are_published_after_storing(self):
        entity = FakeEntity()
        event_source = self._given_an_event_source()
        event_source.save(entity)

        expected_calls = []
        for event in entity._events:
            expected_calls.append(call('FakeEntity', (entity.id, event.serialize())))

        assert event_source.broker.publish.mock_calls == expected_calls

    @patch('inddom.eventsourcing.EntityFactory')
    def ucase_entity_is_loaded_from_event_source(self, entity_factory):
        event_store = Mock()
        event_store.get.return_value = {
            'class': 'AggregateClass',
            'events': (
                (0, 'Event1', '{"arg1": 1}'),
                (1, 'Event2', '{"arg2": 2}'))
            }
        broker = Mock()

        event_source = EventSource(event_store, broker)

        entity = event_source.load(0)

        entity_factory().create.assert_called_with('AggregateClass')
        expected_entity = entity_factory().create.return_value
        assert entity == expected_entity

        entity.Event1.assert_called_with(arg1=1)
        entity.Event2.assert_called_with(arg2=2)

        entity.Event1().apply_on.assert_called_with(entity)
        entity.Event2().apply_on.assert_called_with(entity)
