from dedd.domain import Aggregate, Event


class A_BasicAggregate(Aggregate):
    _next_id = 0

    class CounterInitialized(Event):
        def __init__(self, initial_value=0):
            self.initial_value = 0

        def apply_on(self, aggregate):
            aggregate.counter = 0
            aggregate._id = A_BasicAggregate._next_id
            A_BasicAggregate._next_id += 1

    class CounterIncreased(Event):
        def __init__(self, increment=1):
            self.increment = 1

        def apply_on(self, aggregate):
            aggregate.counter += self.increment

    def create(self):
        self._raise_event(self.CounterInitialized())

    def increase(self, increment):
        self._raise_event(self.CounterIncreased(increment))


class A_SimpleEventObserver:
    def __init__(self):
        self.observed_entities = {}

    def notify_event(self, event, version, from_aggregate):
        id = from_aggregate.id
        if id not in self.observed_entities:
            self.observed_entities[id] = []

        self.observed_entities[id].append((version, event))
