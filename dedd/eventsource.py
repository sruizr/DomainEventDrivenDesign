import abc
import json
from inddom.ddd import EntityFactory


class EventSource:
    def __init__(self, event_store, broker):
        self.event_store = event_store
        self.broker = broker
        self._event_records = dict()
        self._entity_factory = EntityFactory()

    def load(self, entity_id):
        data = self.event_store.get(entity_id)
        entity = self._entity_factory.create(data['class'])
        for event_record in data['events']:
            Event = getattr(entity, event_record[1])
            kwargs = json.loads(event_record[2])
            event = Event(**kwargs)
            event.apply_on(entity)
        return entity

    def save(self, entity):
        if entity.id not in self._event_records:
            entity.subscribe(self)

        for event_record in self._event_records[entity.id]:
            version, event = event_record
            record = (entity.__class__.__name__,
                      entity.id,
                      version,
                      event.__class__.__name__,
                      event.plain_args)

            self.event_store.save(record)
            self.broker.publish(record[0], record[1:])
        self._event_records[entity.id].clear()

    def event_raised(self, entity, event_record):
        if entity.id not in self._event_records:
            self._event_records[entity.id] = []

        self._event_records[entity.id].append(event_record)

    def subscribe_to(self, Entity, subscriber):
        self.broker.subscribe_to(Entity, subscriber)

    def _publish(self):
        pass


class Broker(abc.ABC):
    @abc.abstractmethod
    def publish(self, topic, message):
        pass

    @abc.abstractmethod
    def add_subscriber_to_topic(self, subscriber, topic):
        pass


class ConcurrencyException(Exception):
    pass


class EventStore(abc.ABC):
    @abc.abstractmethod
    def save(self, event_dto):
        """Save event into store
        """
        pass

    @abc.abstractmethod
    def get_events(self, entity_id, from_version=None):
        """Retrieves all events for an entity by its id
        if from_version is given previous events will be ommited
        """
        pass
