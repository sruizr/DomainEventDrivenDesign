import abc


class Entity:
    def __init__(self):
        self._id = None

    @property
    def id(self):
        return self._id

    def __eq__(self, other):
        return self.id == other.id


class Aggregate(Entity, abc.ABC):
    def __init__(self):
        self._history = []
        self._observers = []
        self._version = 0

    def _raise_event(self, event):
        event.apply_on(self)
        self._history.append((self._version, event))
        self._notify_to_every_observer(self._version, event)
        self._version += 1

    def add_observer(self, observer):
        self._observers.append(observer)
        for version, event in self._history:
            self._notify_to_every_observer(version, event)

    def _notify_to_every_observer(self, version, event):
        for observer in self._observers:
            observer.notify_event(event, version, self)


class Value:
    def __init__(self, **kwargs):
        self.__dict__ = kwargs

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        kwargs = ', '.join(
            [f'{key}={value!r}' for key, value in self.__dict__.items()]
        )

        return '{}({})'.format(self.__class__.__name__, kwargs)


class Event(Value, abc.ABC):
    @abc.abstractmethod
    def apply_on(self, aggregate):
        pass
