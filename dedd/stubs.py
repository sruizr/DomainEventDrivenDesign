from dedd.eventsource import EventStore


class OnMemoryEventStore(EventStore):
    def __init__(self):
        self._data = {}

    def save(self, event_dto):
        pass

    def get_events(self, entity_id, from_version):
        pass



class OnMemoryBroker(Broker):
    def __init__(self):
        self._subscribers = []
        self._topics = {}

    def publish(self, topic, message):
        pass

    def add_subscriber_to_topic(self, subscriber, topic):
